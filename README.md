# avc-docker-nginx

Docker image: avcompris/nginx

Usage:

	$ docker run -d --name nginx \
		-p 80:80 \
		-p 443:443 \
		-v /etc/nginx/conf.d:/etc/nginx/conf.d:ro \
		-v /etc/ssl:/etc/ssl:ro \
		-e MUNIN_NODE_NAME=sample-nginx.avcompris.com \
		-e MUNIN_NODE_ALLOW=^172\\.17\\.0\\..+$ \
		avcompris/nginx

Exposed ports are 80, 443, 4949.
