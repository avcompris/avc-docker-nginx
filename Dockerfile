# File: avc-docker-nginx/Dockerfile
#
# Use to build the image: avcompris/nginx, which is nginx + munin-node

FROM nginx:1.23.3
MAINTAINER david.andriana@avantage-compris.com

#-------------------------------------------------------------------------------
#   1. DEBIAN PACKAGES
#-------------------------------------------------------------------------------

RUN apt-get update

# libwww-perl is used by Munin plugins: nginx_*
#
RUN apt-get install -y \
	ntp \
	curl \
	munin-node \
	libwww-perl

#-------------------------------------------------------------------------------
#   2. MUNIN PLUGINS
#-------------------------------------------------------------------------------

RUN ln -s /usr/share/munin/plugins/nginx_request /etc/munin/plugins/
RUN ln -s /usr/share/munin/plugins/nginx_status  /etc/munin/plugins/

#-------------------------------------------------------------------------------
#   3. NGINX CONF
#-------------------------------------------------------------------------------

COPY nginx.conf /etc/nginx/

#-------------------------------------------------------------------------------
#   7. SCRIPTS
#-------------------------------------------------------------------------------

COPY entrypoint.sh .

#-------------------------------------------------------------------------------
# 8. BUILDINFO (RELY ON JENKINS: POPULATED VIA "buildinfo.sh")
#-------------------------------------------------------------------------------

COPY buildinfo /

#-------------------------------------------------------------------------------
#   9. END
#-------------------------------------------------------------------------------

EXPOSE 80
EXPOSE 443
EXPOSE 4949

CMD [ "/bin/bash", "entrypoint.sh" ]




