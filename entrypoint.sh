#/bin/bash

# File: avc-docker-nginx/entrypoint.sh
#
# Add a munin-node to a nginx container.

set -e

MUNIN_NODE_NAME="${MUNIN_NODE_NAME:=nginx}"
sed -i "s/^\s*#\?\s*host_name .*/host_name ${MUNIN_NODE_NAME}/g" /etc/munin/munin-node.conf

if [ -n "${MUNIN_NODE_ALLOW}" ]; then
	echo "allow ${MUNIN_NODE_ALLOW}" >> /etc/munin/munin-node.conf
fi

# start local munin-node
echo "Starting munin-node: ${MUNIN_NODE_NAME}..."
/etc/init.d/munin-node start

# start nginx
echo "Starting nginx..."
nginx -g "daemon off;"